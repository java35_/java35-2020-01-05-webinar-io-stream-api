import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Buffered {

	public static void main(String[] args)  {
		try (FileWriter fw = new FileWriter("D:\\JavaIO\\io.txt")) {
			fw.write("Line 1\n");
			fw.write("Line 2\n");
			fw.write("ABC 2\n");
			fw.write("Line 3\n");
			fw.write("Line 4");
			fw.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// *************************************************************
		FileReader fr = new FileReader("D:\\JavaIO\\io.txt");
		BufferedReader br = new BufferedReader(fr);
		
//		br.lines()
//			.filter(str -> str.matches("Line.*"))
//			.forEach(System.out::println);
		
		String str = br.readLine();
		while(str != null) {
			System.out.println(str);
			str = br.readLine();
		}
		
	}

}
