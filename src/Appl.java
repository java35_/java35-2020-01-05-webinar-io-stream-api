import java.io.FileInputStream;
import java.io.FileOutputStream;

public class Appl {
	public static void main(String[] args) throws Exception {
		FileOutputStream fos = 
				new FileOutputStream("D:\\JavaIO\\io.txt");
		
		String str = "Hello world!";
		fos.write(str.getBytes());
		
		// *************************************************************
		FileInputStream fis = 
				new FileInputStream("D:\\JavaIO\\io.txt");
		byte[] ar = fis.readAllBytes();
		String strFromFile = new String(ar);
		System.out.println(strFromFile);
		
//		System.out.println((char)fis.read());
//		System.out.println((char)fis.read());
//		System.out.println((char)fis.read());
//		System.out.println((char)fis.read());
//		System.out.println(fis.read());
//		System.out.println(fis.read());
		
		
//		for (byte b : ar) {
//			System.out.print((char)b);
//		}
		
	}
}