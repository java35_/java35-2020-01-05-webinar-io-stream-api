package example;


import java.io.Closeable;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Persistent implements Closeable {
	
	PrintWriter pw;
	Scanner scanner;
	
	public Persistent(String fileName) {
		try {
			pw = new PrintWriter(new FileOutputStream(fileName, true));
			scanner = new Scanner(new FileInputStream(fileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void saveToFile(List<EmployeeDao> employees) {
		for (EmployeeDao employeeDao : employees) {
			pw.println(employeeDao.getName() + "|" + employeeDao.getSalary()
					+ "|" + employeeDao.getBirthdate());
		}
		pw.flush();
	}
	
	public List<EmployeeDao> readFromFile() {
		List<EmployeeDao> res = new ArrayList<>();
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			String[] values = line.split("\\|");
			String name = values[0];
			int salary = Integer.parseInt(values[1]);
			LocalDate birthdate = LocalDate.parse(values[2]);
			EmployeeDao employee = 
					new EmployeeDao(name, salary, birthdate);
			res.add(employee);
		}
		return res;
	}

	@Override
	public void close() throws IOException {
		pw.close();
		scanner.close();
	}
}
