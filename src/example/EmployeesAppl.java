package example;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public class EmployeesAppl {
	public static void main(String[] args) {
		EmployeesRepository repo = new EmployeesRepository();
		File file = new File("D:\\JavaIO\\db");
		if (!file.exists()) {
			repo.add(new EmployeeDao("name1", 1, LocalDate.of(2001, 1, 1)));
			repo.add(new EmployeeDao("name2", 2, LocalDate.of(2002, 2, 2)));
			repo.add(new EmployeeDao("name3", 3, LocalDate.of(2003, 3, 3)));
			repo.add(new EmployeeDao("name4", 4, LocalDate.of(2004, 4, 4)));
			repo.add(new EmployeeDao("name5", 5, LocalDate.of(2005, 5, 5)));
		}
		
		System.out.println("-------------------------------");
		List<EmployeeDao> empl = repo.getAll();
		empl.forEach(x -> System.out.println(x));
		
		Persistent persistent = new Persistent("D:\\JavaIO\\db");
//		persistent.saveToFile(empl);
		
		empl = persistent.readFromFile();
		empl.forEach(emploee -> repo.add(emploee));
		
		System.out.println("************************");
		repo.getAll().forEach(x -> System.out.println(x));
		
		try {
			persistent.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}