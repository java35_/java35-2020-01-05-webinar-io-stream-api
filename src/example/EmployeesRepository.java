package example;

import java.util.ArrayList;
import java.util.List;

public class EmployeesRepository {
	private List<EmployeeDao> employees = new ArrayList<>();
	
	public void add(EmployeeDao employeeDao) {
		employees.add(employeeDao);
	}
	
	public List<EmployeeDao> getAll() {
		return new ArrayList<>(employees);
	}
}
