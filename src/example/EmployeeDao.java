package example;

import java.time.LocalDate;

public class EmployeeDao {
	private String name;
	private int salary;
	private LocalDate birthdate;
	
	public EmployeeDao(String name, int salary, LocalDate birthdate) {
		this.name = name;
		this.salary = salary;
		this.birthdate = birthdate;
	}

	public String getName() {
		return name;
	}

	public int getSalary() {
		return salary;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	@Override
	public String toString() {
		return "EmployeeDao [name=" + name + ", salary=" + salary + ", birthdate=" + birthdate + "]";
	}
	
	
}